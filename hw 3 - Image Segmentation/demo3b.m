%Load RGB images From File
load('dip_hw_3.mat','d2a');
load('dip_hw_3.mat','d2b');
rng(1);

Ncut_a=ncut_with_step_one(d2a);
Ncut_b=ncut_with_step_one(d2b);

function nCutValue=ncut_with_step_one(Im)
%Show Original Image
figure;imshow(Im);

%Compute Adjacency Matrix
aff_P=Image2Graph(Im);
%Clustering for 1st Time.
clust_P=myNcuts(aff_P,2);
%Divide Graph According to Cluster and Making New Affinity Matrices
aff_L=aff_P(clust_P==1,clust_P==1);
aff_R=aff_P(clust_P==2,clust_P==2);
%Claclulate nCutValue
nCutValue=calculateNcut(aff_P,clust_P);
%
Cluster_Plot(clust_P,Im);
end

%Visualize Clustering vector in respect of Image dimensions
function Cluster_Plot(cluster_vector,Im)
[M,N,~]=size(Im);
P=reshape(cluster_vector,[M N]);
figure;imagesc(P);
end
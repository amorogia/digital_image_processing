%Recursive Normalized-Cuts
%Load RGB images From File
load('dip_hw_3.mat','d2a');
load('dip_hw_3.mat','d2b');
rng(1);

%Initialize Thresholds
T1=50;
T2=0.79;

%Recursive N-cuts for Image d2a
full_clust_a=start_recursive_ncuts(d2a,T1,T2);
%Recursive N-cuts for Image d2b
full_clust_b=start_recursive_ncuts(d2b,T1,T2);

%Controler Function.
%It is used to call the recursive N-cut method and visualize the clusters
%im     : Image for segmentation.
function full_clust=start_recursive_ncuts(im,T1,T2)
%Global variable for clustering Value addition.
setClusterValue(0);
%Show Original Image
figure;imshow(im);
%Compute Adjacency Matrix
aff_P=Image2Graph(im);
[full_clust,~]=recursive_nCut(aff_P,T1,T2);
%Make full_clust values continious
unique_val=unique(full_clust);
for i=1:size(unique_val)
full_clust(full_clust==unique_val(i))=find(unique_val==unique_val(i));
end
%Plot Clusters 
Cluster_Plot(full_clust,im);
end

%Function for the Recursive Normalized Cut.  
%aff_P      : Adjacency Matrix of Parent. 
%clust_P    : Clust ID for Parent Node
%flag       : Flag that indicates that no more partition of parent node
%             can be made.
function [clust_P,flag]=recursive_nCut(aff_P,T1,T2)
flag=1;
%Begin Clustering
clust_P=myNcuts(aff_P,2);
%Calculate nCutValue & 
nCutValue=calculateNcut(aff_P,clust_P);
%Cut Graph and Find No of Nodes
[aff_L,aff_R]=cutParentGraph(aff_P,clust_P);
[NoOfNodesL,~]=size(aff_L);
[NoOfNodesR,~]=size(aff_R);
if((NoOfNodesL<T1 || NoOfNodesR<T1) || nCutValue>T2)
    flag=0;
    return;
else  
    %Cut Right Node
    [clust_R,flagR]=recursive_nCut(aff_R,T1,T2);
    %If bi-partionion of node can be made set cluster values and put them
    %on the right parent node
    if flagR~=0
    setClusterValue(getClusterValue+2);
    clust_P(clust_P==2)=clust_R+getClusterValue;
    end
    %Cut Left Node
    [clust_L,flagL]=recursive_nCut(aff_L,T1,T2);
    %If bi-partionion of node can be made set cluster values and put them
    %on the left parent node
    if flagL~=0
    setClusterValue(getClusterValue+2);
    clust_P(clust_P==1)=clust_L+getClusterValue;
    end
end
end

%Function that Cuts Parent Graph In 2. 
%aff_P  : Adjacency Matrix of Parent.
%aff_L  : Adjacency Matrix of Cluster Value 1.
%aff_R  : Adjacency Matrix of Cluster Value 2.
%clust_P: Clust ID for Parent Node
function [aff_L,aff_R]=cutParentGraph(aff_P,clust_P)
%Divide Graph According to clust_P 
%Making L,R Affinity Matrices
aff_L=aff_P(clust_P==1,clust_P==1);
aff_R=aff_P(clust_P==2,clust_P==2);
end

%Visualize Clustering vector in respect of Image dimensions
function Cluster_Plot(cluster_vector,Im)
[M,N,~]=size(Im);
P=reshape(cluster_vector,[M N]);
figure;imagesc(P);
end

%___________________________________________________
%Global Cluster Value is used as step for addition.
%It is used to distinguish each new cluster values 
%from their parents cluster values.
function setClusterValue(val)
global x
x = val;
end

function r = getClusterValue
global x
r = x;
end
%____________________________________________________


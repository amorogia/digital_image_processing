function clusterIdx = myNcuts(anAffinityMat,k)
W=anAffinityMat;
[M,N]=size(W);
d = sum(W,2);
D = zeros(M,N);
for i=1:M
    D(i,i)=d(i,1);
end
L=D-W;
[U,~] = eigs(L,D,k,'sr');
clusterIdx=kmeans(U,k);
end
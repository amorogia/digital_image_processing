function clusterIdx = mySpectralClustering(anAffinityMat,k)
W=anAffinityMat;
[M,N]=size(W);
%Sum of Each Row
d = sum(W,2);
D = zeros(M,N);
for i=1:M
    D(i,i)=d(i,1);
end
%Making Lapalacian Matrix
L=D-W;
%Compute k smallest eigenvalues
[U,~] = eigs(L, k, 'sr');
%Clustering Pixels Using k-means
clusterIdx=kmeans(U,k);
end
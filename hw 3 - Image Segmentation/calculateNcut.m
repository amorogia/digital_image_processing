function nCutValue = calculateNcut(anAffinityMat,clusterIdx)
%Get pixel indexes for each Cluster
A=find(clusterIdx==1);
B=find(clusterIdx==2);
%Find association A to V 
assocAV=sum(sum(anAffinityMat(A,:)));
%Find association B to V 
assocBV=sum(sum(anAffinityMat(B,:)));
%Find association A to A 
assocAA=sum(sum(anAffinityMat(A,A)));
%Find association B to B 
assocBB=sum(sum(anAffinityMat(B,B)));
%Compute Nassoc
NassocAB=(assocAA/assocAV)+(assocBB/assocBV);
%Return result
nCutValue=2-NassocAB;
end
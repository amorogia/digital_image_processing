%Load RGB images From File
load('dip_hw_3.mat','d2a');
load('dip_hw_3.mat','d2b');
rng(1);

figure;imshow(d2a);
aff_a=Image2Graph(d2a);
%Normalized-Cut Clustering For aff_a
for i=2:4
Clust_a(:,i-1)=myNcuts(aff_a,i);
Cluster_Plot(Clust_a(:,i-1),d2a);
end

figure;imshow(d2b);
aff_b=Image2Graph(d2b);
%Normalized-Cut Clustering For aff_b
for i=2:4
Clust_b(:,i-1)=myNcuts(aff_b,i);
Cluster_Plot(Clust_b(:,i-1),d2b);
end

%Visualize Clustering vector in respect of Image dimensions
function Cluster_Plot(cluster_vector,Im)
[M,N,n]=size(Im);
P=reshape(cluster_vector,[M N]);
figure;imagesc(P);
end
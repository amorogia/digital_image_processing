%Returns the Affinity Matrix of MxNxn Image
function myAffinityMat = Image2Graph(imIn)
%Find Image Size
[~,~,n]=size(imIn);
%Matrix of all values for pixel in row 
pixel_channels = reshape(imIn, [], n);
%Compute Euclidean disstance between pair of pixels
brightness_distance_vect = pdist(pixel_channels, 'euclidean');
%Make squareform matrix of pairs
brightness_euclidean_distance= squareform(brightness_distance_vect);
%Calculate Weight for each pixel
myAffinityMat=exp(-brightness_euclidean_distance);
end
%Frequency Convolution
function imOut = myConvFreq(imX, imY)
A=double(imX);
B=double(imY);
%find size of each input array.
szX=size(A);
szY=size(B);
%making convolution array size.
m=szX(1)+szY(1)-1;
n=szX(2)+szY(2)-1;
%Zero padding and taking fft of A and B
A=fft2(A,m,n);
B=fft2(B,m,n);
%Multiplication element-by-element of A and B
C = A.*B;
%ImOut has the convolution result in space
imOut=ifft2(C);
end
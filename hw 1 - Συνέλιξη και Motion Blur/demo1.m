%loading data from dip_hw_1.mat
load dip_hw_1.mat;
im_gray = rgb2gray(im_rgb);
im1_rgb = im_rgb(800:900,200:320,:);
im2_rgb = im_rgb(1200:1350,200:300,:);
im1_gray = im_gray(800:900,200:320);
im2_gray = im_gray(1200:1350,200:300);

tic;
%Convolution on im1_gray and im2_gray in space
spat=myConvSpat(im1_gray,im2_gray);
diff=toc;

%Convolution on im1_gray and im2_gray through frequency
freq=myConvFreq(im1_gray,im2_gray);

%Difference in time between Classic Convolution and matrix conv
tic;
myConvSpatClassic(im1_gray,im2_gray);
diff_classic=toc-diff;

%Messuring myConvSpat time 
t1=zeros(1,250);
for N=50:5:250
im1=rand(N,N);
im2=rand(N,N);
tic;
myConvSpat(im1,im2);
t1(N)=toc;
end
figure;
plot(t1);
title('myConvSpat time of NxN image')
xlabel('N')
ylabel('time')
 
%Messuring myConvFreq time
t2=zeros(1,250);
for N=50:5:250
im1=rand(N,N);
im2=rand(N,N);
tic;
myConvFreq(im1,im2);
t2(N)=toc;
end
figure;
plot(t2);
title('myConvFreq time of NxN image')
xlabel('N')
ylabel('time')

%Frequency Convolution
function imOut = myConvFreq(imX, imY)
A=double(imX);
B=double(imY);
%find size of each input array.
szX=size(A);
szY=size(B);
%making convolution array size.
m=szX(1)+szY(1)-1;
n=szX(2)+szY(2)-1;
%Zero padding and taking fft of A and B
A=fft2(A,m,n);
B=fft2(B,m,n);
%Multiplication element-by-element of A and B
C = A.*B;
%ImOut has the convolution result in space
imOut=ifft2(C);
end

%Spatial Convolution
function imOut = myConvSpat(imX,imY)
A=double(imX);
B=double(imY);
%find size of each input array.
szX=size(A);
szY=size(B);
%making the convolution array size.
m=szX(1)+szY(1)-1;
n=szX(2)+szY(2)-1;
%Zero Padding of arrary A.
A = padarray(A,[(szY(1)-1) (szY(2)-1)],'both');
%Unitializing Convolution Array
imOut=zeros(m,n,'double');
%Flipping Array to be convolved with A.
B=fliplr(B);
B=flipud(B);
sz=size(B);
%Convolution of Array A and B.
for n1=1:m
    for n2=1:n
        %multiplication of arrays element by element.
        c=A(n1:(n1-1+sz(1)),n2:(n2-1+sz(2))).*B;
        s=sum(sum(c));
        imOut(n1,n2)=s;
    end
end
end

%Spatial Convolution
function imOut = myConvSpatClassic(imX,imY)
A=double(imX);
B=double(imY);
%find size of each input array.
szX=size(A);
szY=size(B);
%making convolution array size.
m=szX(1)+szY(1)-1;
n=szX(2)+szY(2)-1;
%zero padding
A = padarray(A,[(szY(1)-1) (szY(2)-1)],'post');
B = padarray(B,[(szX(1)-1) (szX(2)-1)],'post');
imOut=zeros(m,n,'double');
for n1=1:m
    for n2=1:n
        for k1=1:n1
            for k2=1:n2
                imOut(n1,n2)=imOut(n1,n2)+A(k1,k2)*B(n1-k1+1,n2-k2+1);
            end
        end
    end
end
end

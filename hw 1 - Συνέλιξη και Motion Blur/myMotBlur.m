function [imOut ,myMotBlurMask] = myMotBlur(imX, mot_vec ,exposure_time)
%Taking b=abs(mot_vec) and ||mot_vec||
b=abs(mot_vec);
meterb=sqrt(b(1,1)^2+b(2,1)^2);
%Round vector 
mot_vec=round(mot_vec);
%Calculating Mask Size : |mot_vec|*exposure_time
y=abs(exposure_time*mot_vec(2,1));
x=abs(exposure_time*mot_vec(1,1));
%Unitializing c-Aproximate Motion Vector
c=zeros(y+1,x+1);
%Unitializing Variables
sum=0;
cos=1;
%Calculating cos between b(mot_vec) and a=[0:x; 0:y;] vector
for i=0:x
    for j=0:y
        %Unitializing a
        a=[i; j;];
        %Taking a^T
        a=transpose(a);
        %||a||
        metera=sqrt(a(1,1)^2+a(1,2)^2);
        %Calculating cos=((a^T)*b)/(||a||*||b||)
        if metera>0
        meter=meterb*metera;
        cos=(a*b);
        cos=cos/meter;
        end
        %if cos>cos(3) put cos value at c(j+1,i+1)'(myMotionBlurMask)'
        if cos > 0.99862953475
            sum=sum+1;
            c(j+1,i+1)=cos;
        end
    end
end
%Divide values with the sum of non zero elements in c
c=(1./(sum))*c;
%Manipulating c(Aproximate Motion Vector) to Make myMotionBlurMask
if mot_vec(1,1)>0 && mot_vec(2,1)>0
    c=fliplr(c);
    c=flipud(c);
end
if mot_vec(1,1)<0 && mot_vec(2,1)>0
    c=flipud(c);
end
if mot_vec(1,1)>0 && mot_vec(2,1)<0
    c=fliplr(c);
end
%Convolving with myMotBlurMask and Making Outputs
myMotBlurMask=c;
imOut=myFilter(imX,myMotBlurMask);
end
%Convolution With mask Filter.
function imOut = myFilter(imX, aMask)
%Initializing variables
imsz=zeros(1,3);
masksz=zeros(1,3);
%Find Image and Mask Sizes
[imsz(1) imsz(2) imsz(3)]=size(imX);
[masksz(1) masksz(2) masksz(3)]=size(aMask);

%if the mask is of 1 channel
if (isequal(masksz(3),1))
    %Taking Each Channel of image and convolve them with mask  
    for i=1:imsz(3)
        a=imX(:,:,i);
        b=myConvSpat(a,aMask);
        imOut(:,:,i)=b;
    end
    %Crop image to original size.
    imOut=imOut(round(masksz(1)./2):end-round((masksz(1)-1)./2),round(masksz(2)./2):end-round((masksz(2)-1)./2),:);
end

%if the mask is of N channels
if not(isequal(masksz(3),1))
    if not(isequal(imsz(3),1))
        %Convolve i channel of image with the i channel of mask
        for i=1:imsz(3)
            imOut(:,:,i)=myConvSpat(imX(:,:,i),aMask(:,:,i));     
        end
        %Crop image to original size.
        imOut=imOut(round(masksz(1)./2):end-round((masksz(1)-1)./2),round(masksz(2)./2):end-round((masksz(2)-1)./2),:);
    %If image is of 1 channel and mask of N channels
    elseif (isequal(imsz(3),1))
        imOut=0;
        msgbox('Invalid Input');
    end
end
end
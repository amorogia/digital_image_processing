load dip_hw_1.mat;
% im_rgb is in workspace
im_gray = rgb2gray(im_rgb);
im1_rgb = im_rgb(800:900,200:320,:);
im2_rgb = im_rgb(1200:1350 ,200:300,:);
im1_gray = im_gray (800:900 ,200:320);
im2_gray = im_gray (1200:1350 ,200:300);

%Computing and Showing im1_rgb
%We use im2double(im1_rgb) function in order to showpicture with imshow
subplot(1,2,1);imshow(im1_rgb);
a=myFilter(im2double(im1_rgb), edge_2d);
subplot(1,2,2);imshow(a);

%Computing mask with im2_gray-Exeption expected.
b=myFilter(im2_gray, edge_3d);

%Computing and Showing im2_rgb
%We use im2double(im2_rgb) function in order to showpicture with imshow
figure();
subplot(1,2,1);imshow(im2_rgb);
c=myFilter(im2double(im2_rgb), blur_3d);
subplot(1,2,2);imshow(c);

%Computing mask with im1_gray-Exeption expected.
d=myFilter(im1_gray, blur_3d);

%Convolution With mask Filter.
function imOut = myFilter(imX, aMask)
%Initializing variables
imsz=zeros(1,3);
masksz=zeros(1,3);
%Find Image and Mask Sizes
[imsz(1) imsz(2) imsz(3)]=size(imX);
[masksz(1) masksz(2) masksz(3)]=size(aMask);

%if the mask is of 1 channel
if (isequal(masksz(3),1))
    %Taking Each Channel of image and convolve them with mask  
    for i=1:imsz(3)
        a=imX(:,:,i);
        b=myConvSpat(a,aMask);
        imOut(:,:,i)=b;
    end
    %Crop image to original size.
    imOut=imOut(round(masksz(1)./2):end-round((masksz(1)-1)./2),round(masksz(2)./2):end-round((masksz(2)-1)./2),:);
end

%if the mask is of N channels
if not(isequal(masksz(3),1))
    if not(isequal(imsz(3),1))
        %Convolve i channel of image with the i channel of mask
        for i=1:imsz(3)
            imOut(:,:,i)=myConvSpat(imX(:,:,i),aMask(:,:,i));     
        end
        %Crop image to original size.
        imOut=imOut(round(masksz(1)./2):end-round((masksz(1)-1)./2),round(masksz(2)./2):end-round((masksz(2)-1)./2),:);
    %If image is of 1 channel and mask of N channels
    elseif (isequal(imsz(3),1))
        imOut=0;
        msgbox('Invalid Input');
    end
end
end

%Spatial Convolution
function imOut = myConvSpat(imX,imY)
A=double(imX);
B=double(imY);
%find size of each input array.
szX=size(A);
szY=size(B);
%making the convolution array size.
m=szX(1)+szY(1)-1;
n=szX(2)+szY(2)-1;
%Zero Padding of arrary A.
A = padarray(A,[(szY(1)-1) (szY(2)-1)],'both');
%Unitializing Convolution Array
imOut=zeros(m,n,'double');
%Flipping Array to be convolved with A.
B=fliplr(B);
B=flipud(B);
sz=size(B);
%Convolution of Array A and B.
for n1=1:m
    for n2=1:n
        %multiplication of arrays element by element.
        c=A(n1:(n1-1+sz(1)),n2:(n2-1+sz(2))).*B;
        s=sum(sum(c));
        imOut(n1,n2)=s;
    end
end
end
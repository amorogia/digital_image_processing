%loading data from dip_hw_1.mat
load dip_hw_1.mat;
im_gray = rgb2gray(im_rgb);
im1_rgb = im_rgb(800:900,200:320,:);
im2_rgb = im_rgb(1200:1350,200:300,:);
im1_gray = im_gray(800:900,200:320);
im2_gray = im_gray(1200:1350,200:300);

%Motion Blur im1_rgb
%We use im2double(im1_rgb) function in order to showpicture with imshow
figure();imshow(im1_rgb);
[a b]=myMotBlur(im2double(im1_rgb),[-6.5476786; 4;],2);
figure();imshow(a);

%Motion Blur im2_rgb
%We use im2double(im2_rgb) function in order to showpicture with imshow
figure();imshow(im2_gray);
[c d]=myMotBlur(im2double(im2_gray),[6; -4;],1);
figure();imshow(c);

function [imOut ,myMotBlurMask] = myMotBlur(imX, mot_vec ,exposure_time)
%Taking b=abs(mot_vec) and ||mot_vec||
b=abs(mot_vec);
meterb=sqrt(b(1,1)^2+b(2,1)^2);
%Round vector 
mot_vec=round(mot_vec);
%Calculating Mask Size : |mot_vec|*exposure_time
y=abs(exposure_time*mot_vec(2,1));
x=abs(exposure_time*mot_vec(1,1));
%Unitializing c-Aproximate Motion Vector
c=zeros(y+1,x+1);
%Unitializing Variables
sum=0;
cos=1;
%Calculating cos between b(mot_vec) and a=[0:x; 0:y;] vector
for i=0:x
    for j=0:y
        %Unitializing a
        a=[i; j;];
        %Taking a^T
        a=transpose(a);
        %||a||
        metera=sqrt(a(1,1)^2+a(1,2)^2);
        %Calculating cos=((a^T)*b)/(||a||*||b||)
        if metera>0
        meter=meterb*metera;
        cos=(a*b);
        cos=cos/meter;
        end
        %if cos>cos(3) put cos value at c(j+1,i+1)'(myMotionBlurMask)'
        if cos > 0.99862953475
            sum=sum+1;
            c(j+1,i+1)=cos;
        end
    end
end
%Divide values with the sum of non zero elements in c
c=(1./(sum))*c;
%Manipulating c(Aproximate Motion Vector) to Make myMotionBlurMask
if mot_vec(1,1)>0 && mot_vec(2,1)>0
    c=fliplr(c);
    c=flipud(c);
end
if mot_vec(1,1)<0 && mot_vec(2,1)>0
    c=flipud(c);
end
if mot_vec(1,1)>0 && mot_vec(2,1)<0
    c=fliplr(c);
end
%Convolving with myMotBlurMask and Making Outputs
myMotBlurMask=c;
imOut=myFilter(imX,myMotBlurMask);
end

%Convolution With mask Filter.
function imOut = myFilter(imX, aMask)
%Initializing variables
imsz=zeros(1,3);
masksz=zeros(1,3);
%Find Image and Mask Sizes
[imsz(1) imsz(2) imsz(3)]=size(imX);
[masksz(1) masksz(2) masksz(3)]=size(aMask);

%if the mask is of 1 channel
if (isequal(masksz(3),1))
    %Taking Each Channel of image and convolve them with mask  
    for i=1:imsz(3)
        a=imX(:,:,i);
        b=myConvSpat(a,aMask);
        imOut(:,:,i)=b;
    end
    %Crop image to original size.
    imOut=imOut(round(masksz(1)./2):end-round((masksz(1)-1)./2),round(masksz(2)./2):end-round((masksz(2)-1)./2),:);
end

%if the mask is of N channels
if not(isequal(masksz(3),1))
    if not(isequal(imsz(3),1))
        %Convolve i channel of image with the i channel of mask
        for i=1:imsz(3)
            imOut(:,:,i)=myConvSpat(imX(:,:,i),aMask(:,:,i));     
        end
        %Crop image to original size.
        imOut=imOut(round(masksz(1)./2):end-round((masksz(1)-1)./2),round(masksz(2)./2):end-round((masksz(2)-1)./2),:);
    %If image is of 1 channel and mask of N channels
    elseif (isequal(imsz(3),1))
        imOut=0;
        msgbox('Invalid Input');
    end
end
end

%Spatial Convolution
function imOut = myConvSpat(imX,imY)
A=double(imX);
B=double(imY);
%find size of each input array.
szX=size(A);
szY=size(B);
%making the convolution array size.
m=szX(1)+szY(1)-1;
n=szX(2)+szY(2)-1;
%Zero Padding of arrary A.
A = padarray(A,[(szY(1)-1) (szY(2)-1)],'both');
%Unitializing Convolution Array
imOut=zeros(m,n,'double');
%Flipping Array to be convolved with A.
B=fliplr(B);
B=flipud(B);
sz=size(B);
%Convolution of Array A and B.
for n1=1:m
    for n2=1:n
        %multiplication of arrays element by element.
        c=A(n1:(n1-1+sz(1)),n2:(n2-1+sz(2))).*B;
        s=sum(sum(c));
        imOut(n1,n2)=s;
    end
end
end

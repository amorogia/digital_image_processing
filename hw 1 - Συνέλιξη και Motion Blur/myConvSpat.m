%Spatial Convolution
function imOut = myConvSpat(imX,imY)
A=double(imX);
B=double(imY);
%find size of each input array.
szX=size(A);
szY=size(B);
%making the convolution array size.
m=szX(1)+szY(1)-1;
n=szX(2)+szY(2)-1;
%Zero Padding of arrary A.
A = padarray(A,[(szY(1)-1) (szY(2)-1)],'both');
%Unitializing Convolution Array
imOut=zeros(m,n,'double');
%Flipping Array to be convolved with A.
B=fliplr(B);
B=flipud(B);
sz=size(B);
%Convolution of Array A and B.
for n1=1:m
    for n2=1:n
        %multiplication of arrays element by element.
        c=A(n1:(n1-1+sz(1)),n2:(n2-1+sz(2))).*B;
        s=sum(sum(c));
        imOut(n1,n2)=s;
    end
end
end
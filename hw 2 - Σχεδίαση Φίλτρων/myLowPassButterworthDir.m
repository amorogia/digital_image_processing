function filtOut=myLowPassButterworthDir(cutOff,n,M,theta,phi)
filtOut=Dir(M,theta,phi).*myLowPassButterworth(cutOff,n,M);
end

%M      : Dir Matrix Dimensions
%theta  : Angle in Degrees
%phi    : Error Angle in Degrees
function outDirectionBoard=Dir(M,theta,phi)     
%Checking if M is even then reduce by 1.
flag=0;     %Iniatilaizing Even M Flag 
if mod(M,2)==0
    flag=1;
    M=M-1;
end
%Delimit 0<=theta<=180 
theta=mod(theta,180);
%Calculating Angle Borders from phi(error angle) so : low<theta<high 
high=theta+(phi/2);
low=theta-(phi/2);
%Reference Vector parallel with X axis
ref_vec=[0 M/2];
%Initializing Upper upper_dir Matrix "For X-Y axis : dimensions (-M/2:M/2,M/2)"
upper_dir=zeros(round(M/2),M);
%Calculating cos(angle) for 0<=angle<=180
for i=0:round(M/2)-1
    for j=0:M-1
        %Initializing [i j] Vector "For X-Y axis : [y x]" 
        element_vec=[i (j-(M/2))];
        %Calculating cos(theta)=(ref_vec*element_vec)/(|ref_vec|*|element_vec|) Beetween element_vec and ref_vec
        cosAngle = dot(ref_vec,element_vec)/(norm(ref_vec)*norm(element_vec));
        %Calculating angle=arccos(cosAngle) in Degrees
        angle = acosd(cosAngle);
        %Check if (low<=angle<=high)
        if angle>=low && angle<=high
            %Put [(phi/2)-angle_proximity_error] in (i+1,j+1)
            upper_dir(i+1,j+1)=(phi/2)-abs(angle-theta);
        end
        if low<0    %If Lower Angle Boarder is on 4th quadrant
            %Changing theta_weight for angle_proximity_error
            theta_weight=theta+180;
            %Calculating angle in 2nd quadrant instead of 4th
            if angle>=180+low && angle<=180
                %Put [(phi/2)-angle_proximity_error] in (i+1,j+1)
                upper_dir(i+1,j+1)=(phi/2)-abs(angle-theta_weight);
            end
        end
        if high>180 %If High Angle Boarder is on 3rd quadrant
            %Changing theta_weight for angle_proximity_error
            theta_weight=theta-180;
            %Calculating angle in 1st quadrant instead of 3rd
            if angle<=high-180 && angle>=0
                %Put [(phi/2)-angle_proximity_error] in (i+1,j+1)
                upper_dir(i+1,j+1)=(phi/2)-(angle-theta_weight);
            end
        end
    end
end
%Croping (1,:) upper_dir dimension for X-Y 
down_dir=upper_dir(2:end,:);
%Rotate upper_dir 180 degrees
down_dir=rot90(down_dir,2);
%Consolidate upper_dir and down_dir 
dir=[down_dir ; upper_dir];
%If M is even copy last line and column of dir and put it at the end of dir matrix
if flag==1
    dir=[dir ; dir(M,:)];
    dir=[dir dir(:,M)];
end
%Scaling DirectionBoard so max(outDirectionBoard)=1
dir=(dir/max(dir(:)));
%Increase DC to 1;
dir(round(M/2),round(M/2))=1;
outDirectionBoard=dir;
end
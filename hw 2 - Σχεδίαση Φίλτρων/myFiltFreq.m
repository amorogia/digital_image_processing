function imOut=myFiltFreq(imIn,someFilt)
%finding size of input image & Filter
szIm = size(imIn);
szFilt=size(someFilt);
%Zero Padding Image And Centering Filter
v=round(((szFilt(1)-1)/2));
b=round(((szFilt(2)-1)/2));
A=padarray(imIn,[v b],'both');
v=round(((szIm(1)-1)/2));
b=round(((szIm(2)-1)/2));
B=padarray(someFilt,[v b],'both');
%Making k(n1,n2)=(-1)^(n1+n2)
k=makeMatrix(A);
%Making A=A*((-1)^(n1+n2))
A=k.*A;
%Taking fft of A 
A=fft2(A);
%Calculating convolution in Frequency
G=A.*B;
%Reverse signal back to Space
g=ifft2(G);
%Making k(n1,n2)=(-1)^(n1+n2)
k=makeMatrix(g);
g=k.*g;
g=cut(g);
imOut=real(g);
end

%Helper Function That makes matrix(n1,n2)=(-1)^(n1+n2)
function imOut=makeMatrix(matrix)
[m n]=size(matrix);
k=zeros(m,n);
for n1=1:m
    for n2=1:n
    k(n1,n2)=(-1)^(n1+n2);
    end
end
imOut=k;
end

%Helper Function that corps Image
function imOut=cut(matrix)
[m n]=size(matrix);
matrix=matrix(round(m/4):round(3*m/4),round(n/4):round(3*n/4));
imOut=matrix;
end
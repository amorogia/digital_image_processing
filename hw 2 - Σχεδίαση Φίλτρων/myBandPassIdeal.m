function filtOut=myBandPassIdeal(cutOffLow,cutOffHigh,M)
LPidl=myLowPassIdeal(cutOffHigh,M);
HPidl=myHighPassIdeal(cutOffLow,M);
filtOut=LPidl.*HPidl;
end
function filtOut=myLowPassButterworth(cutOff,n,M)
%Initializing
H=zeros(M,M,'double');
Do=cutOff;
%Making The Filter
for u1=1:M
    for u2=1:M
        D=sqrt(((u1-(M/2))^2)+((u2-(M/2))^2));
        D=(D/Do)^(2*n);
        H(u1,u2)=(1+D)^(-1);
    end
end
filtOut=H;
end
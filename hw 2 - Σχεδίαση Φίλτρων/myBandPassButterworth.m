function filtOut=myBandPassButterworth(cutOffLow,cutOffHigh,n,M)
LP=myLowPassButterworth(cutOffHigh,n,M);
HP=myHighPassButterworth(cutOffLow,n,M);
filtOut=LP.*HP;
end
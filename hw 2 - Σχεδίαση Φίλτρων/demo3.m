% 3.1 Directional LowPass Filters

    %3.1.1 myLowPassIdealDir Presentation
        %Constructing LowPass Filters with theta_step=15
        k=0;
        d_LP_Ideal=zeros(500,500,12);
        for theta=0:15:(165)
            k=k+1;
            d_LP_Ideal(:,:,k)=myLowPassIdealDir(50,500,theta,30);
        end       
        LPIdeal=sumDirectionalFilters(d_LP_Ideal);
        figure;
        mesh(LPIdeal);
        title('Sum of Directional Filters for Ideal Low Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(LPIdeal,'Image Filtered with Summed Directional for Ideal LP');

    %3.1.2 myLowPassButterworthDir Presentation
        %Constructing LowPass Filters with theta_step=15
        k=0;
        d_LP_Butterworth=zeros(500,500,12);
        for theta=0:15:(165)
            k=k+1;
            d_LP_Butterworth(:,:,k)=myLowPassButterworthDir(50,5,500,theta,30);
        end
        LPBut=sumDirectionalFilters(d_LP_Butterworth);
        figure;
        mesh(LPBut);
        title('Sum of Directional Filters for Butterwort Low Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(LPBut,'Image Filtered with Summed Directional for Butterworth LP');

    %3.1.3 myLowPassPassGaussDir Presentation
        %Constructing LowPass Filters with theta_step=15
        k=0;
        d_LP_Gauss=zeros(500,500,12);
        for theta=0:15:(165)
            k=k+1;
            d_LP_Gauss(:,:,k)=myLowPassGaussDir(50,500,theta,30);
        end    
        LPGauss=sumDirectionalFilters(d_LP_Gauss);
        figure;
        mesh(LPGauss);
        title('Sum of Directional Filters for Gauss Low Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(LPGauss,'Image Filtered with Summed Directional for Gauss LP');                 

% 3.2.a Directional HighPassDir Filters

    %3.2.a.1 myHighPassIdeal Presentation
        %Constructing HighPass Filters with theta_step=15
        k=0;
        d_HP_Ideal=zeros(500,500,12);
        for theta=0:15:(165)
            k=k+1;
            d_HP_Ideal(:,:,k)=myHighPassIdealDir(150,500,theta,30);
        end  
        HPIdeal=sumDirectionalFilters(d_HP_Ideal);
        figure;
        mesh(HPIdeal);
        title('Sum of Directional Filters for Ideal High Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(HPIdeal,'Image Filtered with Summed Directional for Ideal HP');    

    %3.2.a.2 myHighPassButterworthDir Presentation
        %Constructing HighPass Filters with theta_step=15
        k=0;
        d_HP_Butterworth=zeros(500,500,12);
        for theta=0:15:(165)
            k=k+1;
            d_HP_Butterworth(:,:,k)=myHighPassButterworthDir(150,5,500,theta,30);
        end      
        HPBut=sumDirectionalFilters(d_HP_Butterworth);
        figure;
        mesh(HPBut);
        title('Sum of Directional Filters for Butterworth High Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(HPBut,'Image Filtered with Summed Directional for Butterworth HP');
        
    %3.2.a.3 myHighPassGaussDir Presentation
        %Constructing HighPass Filters with theta_step=15
        k=0;
        d_HP_Gauss=zeros(500,500,12);
        for theta=0:15:(165)
            k=k+1;
            d_HP_Gauss(:,:,k)=myHighPassGaussDir(150,500,theta,30);
        end     
        HPGauss=sumDirectionalFilters(d_HP_Gauss);
        figure;
        mesh(HPGauss);
        title('Sum of Directional Filters for Gauss High Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(HPGauss,'Image Filtered with Summed Directional for Gauss HP');
        
% 3.2.b Directional BandPass Filters

    %3.2.b.1 myBandPassIdealDir Presentation
        %Constructing BandPass Filters with theta_step=15
        k=0;
        d_BP_Ideal=zeros(500,500,12);
        for theta=0:15:(165)
            k=k+1;
            d_BP_Ideal(:,:,k)=myBandPassIdealDir(50,150,500,theta,30);
        end
        BPIdeal=sumDirectionalFilters(d_BP_Ideal);
        figure;
        mesh(BPIdeal);
        title('Sum of Directional Filters for Ideal Band Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(BPIdeal,'Image Filtered with Summed Directional for Ideal BP');

    %3.2.b.2 myBandPassButterworthDir Presentation
        k=0;
        d_BP_Butterworth=zeros(500,500,12);
        for theta=0:15:(165)
            k=k+1;
            d_BP_Butterworth(:,:,k)=myBandPassButterworthDir(50,150,5,500,theta,30);
        end
        BPBut=sumDirectionalFilters(d_BP_Butterworth);
        figure;
        mesh(BPBut);
        title('Sum of Directional Filters for Butterworth Band Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(BPBut,'Image Filtered with Summed Directional for Butterworth BP');
    
    %3.2.b.3 myBandPassGaussDir Presentation
        k=0;
        d_BP_Gauss=zeros(500,500,12);
        for theta=0:15:(165)
            k=k+1;
            d_BP_Gauss(:,:,k)=myBandPassGaussDir(50,150,500,theta,30);
        end
        BPGauss=sumDirectionalFilters(d_BP_Gauss);
        figure;
        mesh(BPGauss);
        title('Sum of Directional Filters for Gauss Band Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(BPGauss,'Image Filtered with Summed Directional for Gauss BP');


%Helper Function to sum Directional Filters
function Out=sumDirectionalFilters(d)
%Finding size of directonal Filters
[M, N, Z]=size(d);
%Initializing sum_of_directional_filters Matrix
D=zeros(M,N);
%Suming Directonial Filters
for k=1:Z
    D=D+d(:,:,k);
end
%Cut DC
D(round(M/2),round(N/2))=D(round(M/2),round(N/2))/Z;
%Return Output
Out=D;
end

%Helper Function to Apply Filter and Depict Results        
function showResults(Filter,string)
%Loading Images From File
load('dip_hw_2.mat','demo2Im1');
load('dip_hw_2.mat','demo2Im2');

%For demo2im1
    %Apply Filter on demo2im1 with myFiltFreq
    Out=myFiltFreq(demo2Im1,Filter);
    %Depict Results of Filtering on demo2im1
    %Original Image
    figure;
    subplot(2,2,1);
    imshow(demo2Im1);
    title('Original Image')
    %Filtered Image
    subplot(2,2,2);
    imshow(Out);
    title(string)
    
%For demo2im2
    %Apply Filter on demo2im2 with myFiltFreq
    Out=myFiltFreq(demo2Im2,Filter);
    %Depict Results of Filtering on demo2im2
    %Original Image
    subplot(2,2,3);
    imshow(demo2Im2);
    title('Original Image')
    %Filtered Image
    subplot(2,2,4);
    imshow(Out);
    title(string)
end
function filtOut=myLowPassGauss(sigma,M)
%Initializing
H=zeros(M,M,'double');
%Making The Filter
for u1=1:M
    for u2=1:M
        D=sqrt(((u1-(M/2))^2)+((u2-(M/2))^2));
        H(u1,u2)=exp((-D^2)/(2*(sigma^2)));
    end
end
filtOut=H;
end
function filtOut=myBandPassGauss(sigmaLow,sigmaHigh,M)
LP=myLowPassGauss(sigmaHigh,M);
HP=myHighPassGauss(sigmaLow,M);
filtOut=LP.*HP;
end
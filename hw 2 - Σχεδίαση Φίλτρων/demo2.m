% 2.1 LowPass Filters

    %2.1.1 myLowPassIdeal Presentation
        %Constructing LowPass Filter
        figure;
        LPIdeal=myLowPassIdeal(50,500);
        mesh(LPIdeal);
        title('Ideal Low Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(LPIdeal,'Image Filtered with Ideal LP');

    %2.1.2 myLowPassButterworth Presentation
        %Constructing LowPass Filter
        figure;
        LPBut=myLowPassButterworth(50,5,500);
        mesh(LPBut);
        title('Butterwort Low Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(LPBut,'Image Filtered with Butterworth LP');

    %2.1.3 myLowPassPassGauss Presentation
        %Constructing LowPass Filter
        figure;
        LPGauss=myLowPassGauss(50,500);
        mesh(LPGauss);
        title('Gauss Low Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(LPGauss,'Image Filtered with Gauss LP');                 

% 2.2.a HighPass Filters

    %2.2.a.1 myHighPassIdeal Presentation
        %Constructing HighPass Filter
        figure;
        HPIdeal=myHighPassIdeal(150,500);
        mesh(HPIdeal);
        title('Ideal High Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(HPIdeal,'Image Filtered with Ideal HP');    

    %2.2.a.2 myHighPassButterworth Presentation
        %Constructing HighPass Filter
        figure;
        HPBut=myHighPassButterworth(150,5,500);
        mesh(HPBut);
        title('Butterworth High Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(HPBut,'Image Filtered with Butterworth HP');
        
    %2.2.a.3 myHighPassGauss Presentation
        %Constructing HighPass Filter
        figure;
        HPGauss=myHighPassGauss(150,500);
        mesh(HPGauss);
        title('Gauss High Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(HPGauss,'Image Filtered with Gauss HP');
        
% 2.2.b BandPass Filters

    %2.2.b.1 myBandPassIdeal Presentation
        figure;
        BPIdeal=myBandPassIdeal(50,150,500);
        mesh(BPIdeal);
        title('Ideal Band Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(BPIdeal,'Image Filtered with Ideal BP');

    %2.2.b.2 myBandPassButterworth Presentation
        figure;
        BPBut=myBandPassButterworth(50,150,5,500);
        mesh(BPBut);
        title('Butterworth Band Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(BPBut,'Image Filtered with Butterworth BP');
    
    %2.2.b.3 myBandPassGauss Presentation
        figure;
        BPGauss=myBandPassGauss(50,150,500);
        mesh(BPGauss);
        title('Gauss Band Pass Filter');
        %Showing Results On demo2Im1 & demo2im2
        showResults(BPGauss,'Image Filtered with Gauss BP');

%Helper Function to Apply Filter and Depict Results        
function showResults(Filter,string)
%Loading Images From File
load('dip_hw_2.mat','demo2Im1');
load('dip_hw_2.mat','demo2Im2');

%For demo2im1
    %Apply Filter on demo2im1 with myFiltFreq
    Out=myFiltFreq(demo2Im1,Filter);
    %Depict Results of Filtering on demo2im1
    %Original Image
    figure;
    subplot(2,2,1);
    imshow(demo2Im1);
    title('Original Image')
    %Filtered Image
    subplot(2,2,2);
    imshow(Out);
    title(string)
    
%For demo2im2
    %Apply Filter on demo2im2 with myFiltFreq
    Out=myFiltFreq(demo2Im2,Filter);
    %Depict Results of Filtering on demo2im2
    %Original Image
    subplot(2,2,3);
    imshow(demo2Im2);
    title('Original Image')
    %Filtered Image
    subplot(2,2,4);
    imshow(Out);
    title(string)
end
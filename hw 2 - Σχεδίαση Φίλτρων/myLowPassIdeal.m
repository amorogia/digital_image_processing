function filtOut=myLowPassIdeal(cutOff,M)
%Initializing
H=ones(M,M,'double');
%Making The Filter
for u1=1:M
    for u2=1:M
        D=sqrt(((u1-(M/2))^2)+((u2-(M/2))^2));
        if D>cutOff
           H(u1,u2)=0; 
        end
    end
end
filtOut=H;
end